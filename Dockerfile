FROM registry.jetbrains.team/p/prj/containers/projector-phpstorm

USER root

RUN true \
# Any command which returns non-zero exit code will cause this shell script to exit immediately:
   && set -e \
# Activate debugging to show execution details: all commands will be printed before execution
   && set -x \
# install packages:
    && apt-get update \
    && apt-get install nodejs  -y \
    && apt-get install npm -y \
#    && apt-get install openjdk-11-jre  -y \
#    && apt-get install openjdk-11-jdk openjdk-11-demo openjdk-11-doc openjdk-11-jre-headless #openjdk-11-source -y \
#    && apt-get install maven -y \
# clean apt to reduce image size:
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apt
