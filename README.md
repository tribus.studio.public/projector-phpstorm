# Projector PHPSTORM Environment

## Getting started
1. Create a new workspace folder with 
   
    ``mkdir workspace``
2. Go into the ``ssl`` folder and generate a new jks keystore; Fill out the information and save the password.
   
   ``keytool -genkey -v -keystore ssl_keystore.jsk -alias alias_name -keyalg RSA -keysize 2048 -validity 10000``
3. Edit ``CHANGEME`` to the password you just entered in ``ssl_properties``
4. Build the container 
   
    ``docker build -t phpstorm-projector-environment .``
5. Change in ``docker-compose.yaml`` the following:
    - a new ``container_name``
    - change ports (number before ``:``)
    - Set ``ORG_JETBRAINS_PROJECTOR_SERVER_HANDSHAKE_TOKEN`` and ``ORG_JETBRAINS_PROJECTOR_SERVER_RO_HANDSHAKE_TOKEN`` to add a password to the connection. Please use for both felds the same password
    
6. Launch the server
   
    ``docker-compose up -d``
8. Open intelliJ
  - In Projector Client (Recommended): https://github.com/JetBrains/projector-client
  - In Browser
    
    ``https://{host}:{mapped port of 8887}/?wss&host={host}&port={mapped port of 8887}&token={entered token in docker-compose}``
